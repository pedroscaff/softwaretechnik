package de.htw.dualtodec;
import static org.junit.Assert.*;
import org.junit.Test;

public class DualToDecTest {
  @Test
  public void evaluatesExpression() {
  	DualToDecimal d2d = new DualToDecimal();
  	String[] test = {"01001000", "01100001", "01101100"};
    char[] converted = d2d.d2d(test);
    assertArrayEquals(new char[]{'H','a','l','\n'}, converted);
  }
}
