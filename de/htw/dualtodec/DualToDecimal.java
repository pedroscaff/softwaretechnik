package de.htw.dualtodec;

public class DualToDecimal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// String dual = "11000";

		// System.out.println(Integer.parseInt(dual, 2));

		String[] array =
			{ "01001000", "01100001", "01101100", "01101100", "01101111",
			  "00100000", "01010111", "01100101", "01101100", "01110100" };
		char[] decArray = d2d(array);
		for (int i=0; i < decArray.length; i++) {
			System.out.print(decArray[i]);
		}
	}

	/* What is the string result of the following representation?
	 *
	 * 01001000 01100001 01101100 01101100 01101111
     * 00100000 01010111 01100101 01101100 01110100
	 */

	// your implementatio should be here :-)
	public static char[] d2d(String[] arr) {
		char[] converted = new char[arr.length + 1];
		int i;
		for (i = 0; i < arr.length; i++) {
			converted[i] = (char) Integer.parseInt(arr[i], 2);
		}
		converted[i] = '\n';
		return converted;
	}
}
