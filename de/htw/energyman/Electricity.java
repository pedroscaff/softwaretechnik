package de.htw.energyman;

import java.util.Random;
import java.util.Date;

/**
 * Electricity subclass to Energy
 *
 * @author Robert
 * @author Pedro
 * @author Paul
 * @version  1.0
 */
public class Electricity extends Energy implements Comparable<Electricity> {

    private Boolean friendly = Boolean.TRUE;

    public Electricity() {
        super();
    }

    public Electricity(String name) {
        super(name);
    }

    public Electricity(String name, float performance, Date date) {
        super(name, performance, date);
    }

    public String compute() {
        return "This is test: " + new Random().nextDouble();
    }

    /**
     * @return  the friendly
     */
    public Boolean getFriendly() {
        return friendly;
    }

    /**
     * @param friendly the friendly to set
     */
    public void setFriendly(Boolean friendly) {
        this.friendly = friendly;
    }

    @Override
    public EnergyType getEnergyType() {
        return EnergyType.ELECTRICITY;
    }

    @Override
    public int compareTo(Electricity o) {
      // TODO Auto-generated method stub
  		return 0;
    }
}
