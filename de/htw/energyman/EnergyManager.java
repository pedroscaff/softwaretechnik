package de.htw.energyman;

import de.htw.util.MagicCSV;
import java.util.ArrayList;
import java.util.Iterator;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

/**
 * Manager class for handling the general stuff (importing, sorting, displaying ...).
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public class EnergyManager {

	static ArrayList<Energy> customerList = new ArrayList<Energy>();
	static SimpleDateFormat formatter = new SimpleDateFormat("MMM");
	static String[] dateStrings = new String[]{"January", "February", "March",
																							"April", "May", "June"};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1 || args[0] == "--help") {
			System.out.print("Usage:\n<prog name> [--help] [CSV path]\n\n--help\tShow this message\nCSV path\tpath to the data csv\n");
			return;
		}
		ArrayList<String[]> liste = MagicCSV.readFile(args[0]);
		// map the *.csv entries to the specified objects (W, G, E)
		// display that magic information - first step - without any statistical computing
		// your code here ...
		//get an Iterator object for ArrayList using iterator() method.
		Iterator itr = liste.iterator();
		// just showing how to use to get elements
		// use hasNext() and next() methods of Iterator to iterate through the elements
		while (itr.hasNext()) {
			String[] element = (String[]) itr.next();
			addCustomer(element);
		}
		for (int i=0; i < customerList.size(); i++) {
			Energy element = customerList.get(i);
			System.out.print("Name: " + element.getName());
			System.out.print(" Perf: " + element.getPerformance());
			if (element instanceof Water)
				System.out.print(" Type: " + ((Water)element).getEnergyType());
			else if (element instanceof Gas)
				System.out.print(" Type: " + ((Gas)element).getEnergyType());
			else if (element instanceof Electricity)
				System.out.print(" Type: " + ((Electricity)element).getEnergyType());
			else
				System.out.println(" Type: unknown");
			System.out.print(" Date: " + element.getDate());
			System.out.println();
		}
		// compute and analyze in a second step (RegressionsFunction, MagicFunction)
		// Function regression = new RegressionsFunction();
		// Function magic = new MagicFunction();
		// your further code here ...

		// display the final output
		// your code here ...
	}

	// create further methods if necessary
	private static void addCustomer(String[] element) {
		switch (element[7]) {
			case "G": {
				for (int i=1; i < 7; i++) {
					try {
						Date date = formatter.parse(dateStrings[i-1]);
						Energy customer = new Gas(element[0], Float.parseFloat(element[i]), date);
						customerList.add(customer);
						} catch (ParseException e) {
							e.printStackTrace();
					}
				}
				break;
			}
			case "E": {
				for (int i=1; i < 7; i++) {
					try {
						Date date = formatter.parse(dateStrings[i-1]);
						Energy customer = new Electricity(element[0], Float.parseFloat(element[i]), date);
						customerList.add(customer);
						} catch (ParseException e) {
							e.printStackTrace();
					}
				}
				break;
			}
			case "W": {
				for (int i=1; i < 7; i++) {
					try {
						Date date = formatter.parse(dateStrings[i-1]);
						Energy customer = new Water(element[0], Float.parseFloat(element[i]), date);
						customerList.add(customer);
						} catch (ParseException e) {
							e.printStackTrace();
					}
				}
				break;
			}
		}
	}
}
