package de.htw.fib;

import java.util.*;
import java.math.BigInteger;

public class Fibonacci {

    private static int maxzahl, call;
    private static ArrayList<BigInteger> list = new ArrayList<BigInteger>();
    static {
        list.add(BigInteger.valueOf(1));
        list.add(BigInteger.valueOf(1));
    }

    /**
     * @param args - args[1] is the calculation method,
     * args[0] is the number of numbers to calculate
     */
    public static void main (String [] args) {
        if (args.length < 2) {
            System.out.println("Usage: <de.htw.fibonacci> [<max>] [<mode>]\nArguments:\n\t<max>\t- numbers to calculate\n\t<mode>\t- calculation method\n\t\t  0 for iterative\n\t\t  1 for recursive");
            return;
        }
        maxzahl = Integer.parseInt(args[0]);
        Fibonacci seq = new Fibonacci();
        // if iterative
        if (0 == Integer.parseInt(args[1])) {
            System.out.println("Iterativ!");
            BigInteger [] fib = seq.iterativ(maxzahl);
            for(int i = 0; i < fib.length; i++)
            {
                // printPair((i+1), fib[i]);
                printBinary((i+1), fib[i]);
            }
        }
        // if recursive
        else {
            System.out.println("Recursive!");
            seq.recursive(maxzahl);
            for(int i = 1; i < list.size()+1; i++) {
                // printPair(i, list.get(i-1));
                printBinary(i, list.get(i-1));
            }
        }
    }

    public ArrayList<BigInteger> getRecList() {
        return this.list;
    }

    /**
     * @param max - highest index of the fibonacci list
     */
    public static void recursive(int max) {
        int p = list.size();
        if (p < max) {
            BigInteger newval = list.get(p-1).add(list.get(p-2));
            list.add(newval);
            recursive(max);
        }
    }

    public static void printPair(int a, BigInteger b) {
        System.out.print(a);
        System.out.print(" -> ");
        System.out.println(b);
    }

    public static String toBinary(Integer dec) {
        String bin = new String();
        while (dec > 0) {
            int res = dec % 2;
            dec = dec / 2;
            bin = res + bin;
        }
        return bin;
    }

    public static void printBinary(int a, BigInteger b) {
        System.out.print(a);
        System.out.print("\t->\t");
        System.out.print(b);
        System.out.print("\t->\t");
        System.out.println(String.format("%13s", toBinary(b.intValue())).replace(' ','0'));
    }

    /**
     * @param maxzahl - max number of iteractions
     */
    public BigInteger[] iterativ(int maxzahl) {
        BigInteger [] fib = new BigInteger [maxzahl];
        fib[0] = BigInteger.valueOf(1);
        fib[1] = BigInteger.valueOf(1);
        for (int i = 2; i < maxzahl; i++) {
            fib[i] = fib[i-1].add(fib[i-2]);
        }
        return fib;
    }
}
