package de.htw.fib;
import java.util.*;
import static org.junit.Assert.*;
import org.junit.Test;
import java.math.BigInteger;

public class FibonacciTest {

	BigInteger[] ite;
	BigInteger[] rec2 = new BigInteger[10];
	ArrayList<BigInteger> rec;
	BigInteger[] fib = { BigInteger.valueOf(1), BigInteger.valueOf(1), BigInteger.valueOf(2),
											 BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(8),
											 BigInteger.valueOf(13), BigInteger.valueOf(21), BigInteger.valueOf(34), BigInteger.valueOf(55) };
	Fibonacci seq = new Fibonacci();

	@Test
	public void evaluatesRecursive() {
		int i;
		seq.recursive(10);
		rec = seq.getRecList();
		for(i = 0; i<rec.size(); i++) {
			rec2[i] = rec.get(i);
		}
		assertArrayEquals(fib, rec2);
  }

  @Test
	public void evaluaterIterativ() {
		ite = seq.iterativ(10);
		assertArrayEquals(fib, ite);
	}
}
