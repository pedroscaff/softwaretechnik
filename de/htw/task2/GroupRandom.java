package de.htw.task2;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Random;

class GroupRandom {

	Boolean loop = true;
	String fileName = new String("de/htw/task2/groups.csv");
	String[] groups;
	BufferedReader in;

	public static void main(String[] args) {
		GroupRandom grp = new GroupRandom();
		grp.getGroups();
		grp.getRandomGroup();
	}

	private void getRandomGroup() {
		Random r = new Random();
		int Low = 0;
		int High = 10;
		int R = r.nextInt(High-Low) + Low;
		while(loop) {
			System.out.println(groups[R]);
			R = r.nextInt(High-Low) + Low;
		}
	}

	private void getGroups() {
		try {
			in = new BufferedReader(new FileReader(fileName));
			// skip to second line
			// first line only contains column names
			in.readLine();
			groups = new String[11];
			int i = 0;
            while((groups[i] = in.readLine()) != null) i++;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}