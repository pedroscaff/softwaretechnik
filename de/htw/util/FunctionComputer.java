package de.htw.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Erik
 *
 */
public class FunctionComputer {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Function regression = new RegressionsFunction();
		Function magic = new MagicFunction();
		List<Function> functionList = new ArrayList<Function>();
		functionList.add(regression);
		functionList.add(magic);

		double[] xValues = {1d, 2d, 3d};
		double[] yValues = {2d, 3d, 3d};

		for(Function f : functionList)
			System.out.println(f.compute(xValues, yValues));
	}

}
