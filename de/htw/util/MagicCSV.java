package de.htw.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * Utility class for reading in the *.csv file and build up some Energy's.
 *
 * @author Erik Mautsch
 * @author Your Name
 * @version 1.0
 */
public class MagicCSV {

	public MagicCSV() {
	}

	public static ArrayList<String[]> readFile(String filename) {
		ArrayList<String[]> liste = new ArrayList<String[]>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(filename)));
			// your further code here ...
			// think about the usage of EnergyIterable.java - see Moodle for details
			// EnergyIterable: https://moodle.htw-berlin.de/pluginfile.php/212566/mod_resource/content/1/EnergyIterable.java
			// EnergyIterableTest: https://moodle.htw-berlin.de/pluginfile.php/212568/mod_resource/content/3/EnergyIterableTest.java
			// skip to second line
			// first line only contains column names
			br.readLine();
			String line;
			String[] lineSplited;
			// read all lines of csv
			while((line = br.readLine()) != null) {
					// split csv fields
					lineSplited = line.split(";");
					// do not add if field is null (blank)
					if (lineSplited.length != 0)
						liste.add(lineSplited);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return liste;
	}

	public void sort(ArrayList<?> customerList) {
		// ...
	}

}
