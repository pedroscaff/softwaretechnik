#!/bin/bash

# use "mvn clean package && ./run.sh"
# java -cp target/energyman-2.0-SNAPSHOT.jar:$HOME/.m2/repository/org/apache/commons/commons-math3/3.5/commons-math3-3.5.jar de.htw.energyman.EnergyManager "src/main/resources/EnergyForecast-Unicode(UTF-8).csv"

# use "mvn clean compile && ./run.sh"
mvn exec:java -Dexec.mainClass="de.htw.energyman.EnergyManagerGUI" -Dexec.classpathScope=runtime -Dexec.args="src/main/resources/EnergyForecast-Unicode(UTF-8).csv"

# to package as single jar with dependencies included
# mvn clean compile assembly:single
