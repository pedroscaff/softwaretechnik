package de.htw.energyman;

import de.htw.energyman.energies.*;
import de.htw.energyman.util.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.lang.StringBuffer;
import java.lang.Exception;

/**
 * Customer Class to organize Customer Data by EnergyType
 *
 * @author Robert
 * @author Pedro
 * @author Paul
 * @version 0.1
 */
public class Customer {

    private HashMap<String, String> infoMap = new HashMap<String, String>();
    private HashMap<EnergyType, ArrayList<Energy>> energyMap = new HashMap<EnergyType, ArrayList<Energy>>();

    public Customer(String name) {
        setName(name);
    }

    public void setName(String name) {
        infoMap.put("name", name);
    }

    public String getName() {
        return infoMap.get("name");
    }

    public void addEnergyData(EnergyType type, Energy data) {
        if (energyMap.containsKey(type)) {
            energyMap.get(type).add(data);
        } else {
            energyMap.put(type, new ArrayList<Energy>());
            //
            // might be possible to call function recursively?
            energyMap.get(type).add(data);
        }
    }

    public ArrayList<Energy> getEnergyDataList() throws Exception {
        if (energyMap.isEmpty()) {
            throw new Exception("No data");
        } else {
            ArrayList<Energy> ret = new ArrayList<Energy>();
            for (ArrayList<Energy> ale : energyMap.values()) {
                ret.addAll(ale);
            }
            return ret;
        }
    }

    public ArrayList<Energy> getEnergyDataList(EnergyType type) throws Exception {
        if (energyMap.isEmpty()) {
            throw new Exception("No data");
        } else {
            return energyMap.get(type);
        }
    }

    public String toString() {
        StringBuffer out = new StringBuffer("#### Start of Customer Information ####\n");

        out.append("Name: " + this.getName() + "\n");
        out.append("----------\n");

        out.append("Water data: \n");
        try {
            for (Energy e : this.getEnergyDataList(EnergyType.WATER)) {
                out.append(e.toString() + "\n");
            }
        } catch (Exception e) {
            out.append(e.getMessage()+"\n");
        }
        out.append("----------\n");

        out.append("Gas data: \n");
        try {
            for (Energy e : this.getEnergyDataList(EnergyType.GAS)) {
                out.append(e.toString() + "\n");
            }
        } catch (Exception e) {
            out.append(e.getMessage()+"\n");
        }
        out.append("----------\n");

        out.append("Electricity data: \n");
        try {
            for (Energy e : this.getEnergyDataList(EnergyType.ELECTRICITY)) {
                out.append(e.toString() + "\n");
            }
        } catch (Exception e) {
            out.append(e.getMessage()+"\n");
        }
        out.append("----------\n");

        out.append("#### End of Customer Information ####\n");

        return out.toString();
    }
}