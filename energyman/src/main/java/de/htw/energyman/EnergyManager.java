package de.htw.energyman;

import de.htw.energyman.energies.*;
import de.htw.energyman.util.*;

import java.lang.Exception;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
// import java.util.Map.Entry;
import java.util.Set;

/**
 * Manager class for handling the general stuff (importing, sorting, displaying ...).
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public final class EnergyManager {

	private static HashMap<String, Customer> customerMap = new HashMap<String, Customer>();

	static String[] dateStrings = new String[]{"January", "February", "March", "April", "May", "June"};
	static String usingYear = "2015";
	private static MagicCSV csv = null;
	private static String filename;

	public EnergyManager() {
		this.filename = "null";
		this.csv = null;
	}

	public EnergyManager(String filename) {
		// save filename just in case
		loadCsv(filename);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1 || args[0] == "--help") {
			System.out.print("Usage:\n\tde.htw.EnergyManager [--help] <csv>\nArguments:\n\t--help\tshow this message\n\tcsv\tpath to the data csv\n");
			return;
		}
		EnergyManager manager = new EnergyManager(args[0]);
		// map the *.csv entries to the specified objects (W, G, E)
		manager.energizeCSV();
		// display that magic information - first step - without any statistical computing
		System.out.println("#== Raw Data Dump ===================================#\n");
		System.out.println(manager.getCustomersEnergyData());

		// manager.printCustomersUgly();
		// seperate output
		// System.out.println("#== Data Prediction  ===================================#\n");
		// compute and analyze in a second step (RegressionsFunction, MagicFunction)
		// Function regression = new RegressionsFunction();
		// Function magic = new MagicFunction();
		// your further code here ...
		// print regression results:
		// for (Map.Entry<String, Customer> me : manager.customerMap.entrySet()) {
		// 	manager.getCustomerStatistics(me.getKey(), EnergyType.WATER);
		// 	manager.getCustomerStatistics(me.getKey(), EnergyType.GAS);
		// 	manager.getCustomerStatistics(me.getKey(), EnergyType.ELECTRICITY);
		// 	System.out.println("--------------------");

		// }

		// seperate regression output from sort ouput
		// System.out.println("#== Data Sort ===================================#\n");

		// sort data:
		// ArrayList<Float> schulzeSorted = manager.sort("Schulze");
		// System.out.println("from highest to lowest value:");
		// for (Float f : schulzeSorted) {
		// 	System.out.println(f.toString());
		// }

		// TODO:
		// Comparable interfaces
		// sorting (performance)
	}

	/**
	 * Passes the filename to an instance of MagicCSV to load raw data and creates Energy objects
	 * @param filename Path to the CSV file to load
	 */
	public static void loadCsv(String filename) {
		filename = filename;
		csv = new MagicCSV(filename);
	}

	/**
	 * Getter method for customermap member
	 * @return HashMap<String, Customer>
	 */
	public static  ArrayList<Energy> getCustomersEnergyData() {
		ArrayList<Energy> edl = new ArrayList<Energy>();
		for (Customer c : customerMap.values()) {
			try {
				edl.addAll(c.getEnergyDataList());
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return edl;
	}

	public static void processCsv() {
		energizeCSV();
	}

	public static HashMap<String, Double[]> getPredictionData() {
		MagicFunction magic = new MagicFunction();
		RegressionsFunction regression = new RegressionsFunction();
		double[] x = new double[6];
		double[] y = new double[6];
		HashMap<String, Double[]> foreCast = new HashMap<String, Double[]>();
		int i;
		try {
			for (Customer c : customerMap.values()) {
				i=0;
				for (Energy e : c.getEnergyDataList()) {
					x[i] = i;
					y[i] = e.getPerformance();
					i++;
				}
				Double[] da = { magic.computeValue(x, y), regression.computeValue(x, y) };
				foreCast.put(c.getName(), da);
			}
		} catch (Exception e) {
			//System.out.println(">>>>" + e.getMessage());
			e.printStackTrace();
		}
		return foreCast;
	}

	/********/

	private void getCustomerStatistics(String name, EnergyType type) {
		Function magic = new MagicFunction();
		Function regression = new RegressionsFunction();
		double[] x = new double[6];
		double[] y = new double[6];
		int i = 0;
		try {
			for (Energy c : customerMap.get(name).getEnergyDataList(type)) {
				if (c.getName().equals(name)) {
					x[i] = i;
					y[i] = c.getPerformance();
					i++;
				}
			}
			System.out.println(type+" :");
			regressionReport(name, magic, regression, x, y);
			//
		} catch (Exception e) {
			System.out.println(type + " :" + e.getMessage());
		}
	}

	public void regressionReport(String customerName, Function mf, Function rf, double[] x, double[] y) {
		//
		System.out.println("\tClient: " + customerName);
		Map<Date, Double> foreCast = mf.compute(x, y);
		System.out.println("\tMAGIC FUNCTION MEAN");
		System.out.println("\t\t" + Arrays.toString(foreCast.entrySet().toArray()));
		foreCast = rf.compute(x, y);
		System.out.println("\tREGRESSION FUNCTION PREDICTION");
		System.out.println("\t\t" + Arrays.toString(foreCast.entrySet().toArray()));
	}

	/**
	* Print all customers informations - DEBUG output
	*/
	public void printCustomersUgly() {
		try {
			for (Map.Entry<String, Customer> me : customerMap.entrySet()) {
				System.out.println(me.getValue().toString());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	* Convert csv to Energy objects
	*/
	private static void energizeCSV() {
		// skip first row
		csv.readRow();
		String line;
		while ((line = csv.readRow()) != null) {
			addCustomer(line);
		}
	}

	/**
	* Add a Customer to the customerList.
	* @param parseString String that gets parsed as a Customer
	*/
	private static void addCustomer(String parseString) {
		EnergyIterable itr = new EnergyIterable(parseString);
		while (itr.hasNext()) {
			try {
				Customer newCustomer = findAndInstantiate(itr);
				customerMap.put(newCustomer.getName(), newCustomer);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	* Iterate through csv row and get all customer information
	* create Energy objects for customer
	*/
	private static Customer findAndInstantiate(EnergyIterable itr) throws Exception {
		String name = itr.next();
		Customer newCustomer = new Customer(name);

		ArrayList<String> performance = new ArrayList<String>();
		while (itr.getTokenCount() > 1) {
			performance.add(itr.next());
		}
		String type = itr.next();
		int i = 0;
		for (String p : performance) {
			switch (type) {
				case "W": {
					newCustomer.addEnergyData(EnergyType.WATER, new Water(name, Float.parseFloat(p), usingYear+"-"+dateStrings[i++]));

					break;
				}
				case "G": {
					newCustomer.addEnergyData(EnergyType.GAS, new Gas(name, Float.parseFloat(p), usingYear+"-"+dateStrings[i++]));
					break;
				}
				case "E": {
					newCustomer.addEnergyData(EnergyType.ELECTRICITY, new Electricity(name, Float.parseFloat(p), usingYear+"-"+dateStrings[i++]));
					break;
				}
				default: {
					i++;
					throw new Exception("!csv entry has invalid type ''" + type + "''");
				}
			}
		}

		return newCustomer;
	}

	/**
	 * Sorts the monthly energy data per customer in descending order by performance
	 * @param customer customer to sort data for
	 * @return  Returns a ArrayList<Float> with the sorted performance data
	 */
	// private ArrayList<Float> sort(String customer) {
	// 	System.out.println("sorted performance data for '"+customer+"'");
	// 	ArrayList<Float> perfList = this.createSingleCustomerList(customer);
	// 	perfList.sort(Collections.reverseOrder());
	// 	return perfList;
	// }

	// private ArrayList<Float> createSingleCustomerList(String customer) {
	// 	// create single customer list
	// 	ArrayList<Float> singleCustomerList = new ArrayList<Float>();
	// 	// loop over customerList and get customer data
	// 	for (Energy e : customerList) {
	// 		if (customer.equals(e.getName())) {
	// 			singleCustomerList.add(e.getPerformance());
	// 		}
	// 	}
	// 	return singleCustomerList;
	// }

}
