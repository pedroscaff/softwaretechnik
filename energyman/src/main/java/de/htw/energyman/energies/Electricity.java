package de.htw.energyman.energies;

import java.util.Random;

/**
 * Electricity subclass to Energy
 *
 * @author  Robert
 * @author Pedro
 * @author Paul
 * @version  1.0
 */
public class Electricity extends Energy implements Comparable<Electricity> {

    private Boolean friendly = Boolean.TRUE;

    public Electricity() {
        super();
    }

    public Electricity(String name) {
        super(name);
    }

    public Electricity(String name, Float performance, String date) {
      super(name, performance, date);
    }

    public String compute() {
        return "This is test: " + new Random().nextDouble();
    }

    /**
     * @return  the friendly
     */
    public Boolean getFriendly() {
        return friendly;
    }

    /**
     * @param friendly the friendly to set
     */
    public void setFriendly(Boolean friendly) {
        this.friendly = friendly;
    }

    @Override
    public EnergyType getEnergyType() {
        return EnergyType.ELECTRICITY;
    }

    @Override
    public String toString() {
        return "Electricity [name=" + name + ", performance=" + performance
                + ", date=" + date + "]";
    }

    @Override
    public int compareTo(Electricity o) {
        Float otherperf = o.getPerformance();
        if (this.performance > otherperf) {
            return 1;
        }
        else if (this.performance < otherperf) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
