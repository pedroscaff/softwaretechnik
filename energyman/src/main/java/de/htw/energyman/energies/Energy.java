package de.htw.energyman.energies;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;


/**
 * An abstract class, that holds common stuff.
 *
 * <ul>
 * <li>name
 * <li>performance
 * <li>date
 * </ul>
 * <p>
 *
 * @author Erik Mautsch
 *
 */
public abstract class Energy {

	protected String name;
	protected Float performance;
	protected Date date;
	protected static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM");

	protected Energy() {
		// TODO Auto-generated constructor stub
	}

	protected Energy(String name, Float performance, String date) {
		this.name = name;
		this.performance = performance;
		this.date = parseDate(date);
	}

	/**
	 * @param name
	 */
	protected Energy(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public abstract String compute();

	/**
	 * @return EnergyType
	 */
	protected abstract EnergyType getEnergyType();


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the performance
	 */
	public Float getPerformance() {
		return performance;
	}

	/**
	 * @param performance
	 *            the performance to set
	 */
	public void setPerformance(Float performance) {
		this.performance = performance;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	// utiliy:
	public Date parseDate(String dateStr) {
		Date d = null;
		try {
			d = formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d;
	}

	public String getEnergyTypeAsString() {
		return this.getEnergyType().name();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Energy [name=" + name + ", performance=" + performance
				+ ", date=" + date + "]";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((performance == null) ? 0 : performance.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Energy other = (Energy) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (performance == null) {
			if (other.performance != null)
				return false;
		} else if (!performance.equals(other.performance))
			return false;
		return true;
	}

}
