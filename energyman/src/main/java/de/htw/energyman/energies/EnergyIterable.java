package de.htw.energyman.energies;

import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * An initial utility class for iterate over an line of String ... .
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public class EnergyIterable implements Iterable<String>, Iterator<String> {
    private StringTokenizer st;

    public EnergyIterable(String s) {
        st = new StringTokenizer(s, ";");
    }

    // method from Iterable
    public Iterator<String> iterator() {
        return this;
    }

    public int getTokenCount() {
      return st.countTokens();
    }

    // method from Iterator
    public boolean hasNext() {
        return st.hasMoreTokens();
    }

    public String next() {
        return st.nextToken();
    }

    public void remove() {
        // No remove.
    }
}
