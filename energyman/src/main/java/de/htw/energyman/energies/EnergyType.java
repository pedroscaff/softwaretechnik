package de.htw.energyman.energies;

/**
 * ENUM for different EnergyType's.
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public enum EnergyType {
	WATER, GAS, ELECTRICITY;
}
