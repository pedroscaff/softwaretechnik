package de.htw.energyman.energies;

import java.util.Random;

/**
 * Gas subclass to Energy
 *
 * @author  Robert
 * @author Pedro
 * @author Paul
 * @version  1.0
 */
public class Gas extends Energy implements Comparable<Gas> {

    private Boolean friendly = Boolean.FALSE;

    public Gas() {
        super();
    }

    public Gas(String name) {
        super(name);
    }

    public Gas(String name, Float performance, String date) {
      super(name, performance, date);
    }

    public String compute() {
        return "This is test: " + new Random().nextDouble();
    }

    /**
     * @return  the friendly
     */
    public Boolean getFriendly() {
        return friendly;
    }

    /**
     * @param friendly the friendly to set
     */
    public void setFriendly(Boolean friendly) {
        this.friendly = friendly;
    }

    @Override
    public EnergyType getEnergyType() {
        return EnergyType.GAS;
    }

    @Override
    public String toString() {
        return "Gas [name=" + name + ", performance=" + performance
                + ", date=" + date + "]";
    }

    @Override
    public int compareTo(Gas o) {
        Float otherperf = o.getPerformance();
        if (this.performance > otherperf) {
            return 1;
        }
        else if (this.performance < otherperf) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
