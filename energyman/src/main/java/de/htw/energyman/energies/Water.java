package de.htw.energyman.energies;

import java.util.Random;

/**
 * Simple bean / domain class for holding the specified
 * information for a concret Energy subclass.
 *
 * @author      Erik Mautsch
 * @author      Your Name
 * @version     1.0
 */
public class Water extends Energy implements Comparable<Water>{

	// You're an environmentally friendly solution
	private Boolean friendly = Boolean.TRUE;

	public Water() {
		super();
	}

	public Water(String name) {
		super(name);
	}

	public Water(String name, Float performance, String date) {
		super(name, performance, date);
	}

	public String compute() {
		return "Special computing in Water: " + new Random().nextDouble();
	}


	/**
	 * @return the friendly
	 */
	public Boolean getFriendly() {
		return friendly;
	}

	/**
	 * @param friendly the friendly to set
	 */
	public void setFriendly(Boolean friendly) {
		this.friendly = friendly;
	}

	@Override
	public EnergyType getEnergyType() {
		return EnergyType.WATER;
	}

	@Override
	public String toString() {
		return "Water [name=" + name + ", performance=" + performance
				+ ", date=" + date + "]";
	}

	@Override
    public int compareTo(Water o) {
        Float otherperf = o.getPerformance();
        if (this.performance > otherperf) {
            return 1;
        }
        else if (this.performance < otherperf) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
