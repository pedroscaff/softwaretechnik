package de.htw.energyman.gui;

import de.htw.energyman.EnergyManagerGUI;

import java.lang.Thread;
import javafx.application.Application;

public class GUIThread extends Thread {
    private String[] args;
    public GUIThread(String[] args) {
      this.args = args;
    }
    public void run() {
      Application.launch(EnergyManagerGUI.class, args);
    }
}
