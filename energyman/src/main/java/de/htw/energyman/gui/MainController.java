package de.htw.energyman.gui;

import de.htw.energyman.EnergyManager;
import de.htw.energyman.energies.Energy;
import de.htw.energyman.util.CSVThread;

import java.io.File;
// import java.util.HashMap;
import java.util.Map;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.ObservableList;
import org.controlsfx.control.StatusBar;
import java.lang.InterruptedException;



public class MainController {

    private File csvFile;
    private Stage mainStage;
    private ObservableList<Energy> tableContent;

    public MainController() {
        tableView = new TableView<Energy>();

        energyCol = new TableColumn<Energy,String>("Energy");
        energyCol.setCellValueFactory(new PropertyValueFactory("EnergyTypeAsString"));
        nameCol = new TableColumn<Energy,String>("Name");
        nameCol.setCellValueFactory(new PropertyValueFactory("name"));
        perfCol = new TableColumn<Energy,String>("Performance");
        perfCol.setCellValueFactory(new PropertyValueFactory("performance"));
        dateCol = new TableColumn<Energy,String>("Date");
        dateCol.setCellValueFactory(new PropertyValueFactory("date"));
    }

    public void setStage(Stage stage) {
         this.mainStage = stage;
    }
    // public void setTableContent(Collection<Energy> data) {
    //     this.tableContent = data;
    // }

    public String getCsvPath() {
        return csvFile.getPath();
    }

    /***/

    @FXML private TableView<Energy> tableView;
    @FXML private TableColumn<Energy,String> energyCol;
    @FXML private TableColumn<Energy,String> nameCol;
    @FXML private TableColumn<Energy,String> perfCol;
    @FXML private TableColumn<Energy,String> dateCol;
    @FXML private StatusBar statusbar;
    @FXML private TextArea output;

    @FXML
    protected void handleOpenCSVButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        this.csvFile = fileChooser.showOpenDialog(mainStage);
        // this.csvFile = new File("/Users/robert/devel/CE-HTW/softwaretechnik/_group_repo/energyman/src/main/resources/EnergyForecast-Unicode(UTF-8).csv");
        if (csvFile != null) {
            statusbar.setText("CSV: "+csvFile.getName());
            CSVThread loadCsv = new CSVThread(csvFile.getPath());
            loadCsv.start();
            try {
              loadCsv.join(); // wait for loading to complete
            }
            catch (InterruptedException e) {
              System.out.println(e.getMessage());
            }
            tableContent = javafx.collections.FXCollections.observableArrayList(EnergyManager.getCustomersEnergyData());
            tableView.setItems(tableContent);
            tableView.getColumns().setAll(energyCol, nameCol, perfCol, dateCol);
       }
    }

    @FXML
    protected void handlePredictButtonAction(ActionEvent event) {
        statusbar.setText("Statistics");
        for (Map.Entry<String, Double[]> me : EnergyManager.getPredictionData().entrySet()) {
            output.appendText(me.getKey()+"\n");
            output.appendText("Mean: "+me.getValue()[0]+"\n");
            output.appendText("Prediction: "+me.getValue()[1]+"\n");
            output.appendText("--------------------\n\n");
        }
        // output.appendText( EnergyManager.regressionPrediction(EnergyManager.getCustomersEnergyData()) );
        // System.out.println( EnergyManager.regressionPrediction(EnergyManager.getCustomersEnergyData()) );
    }

    @FXML
    protected void handleClearButtonAction(ActionEvent event) {
        statusbar.setText("Clear");
        output.setText("");
    }
}
