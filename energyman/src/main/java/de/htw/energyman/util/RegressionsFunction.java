package de.htw.energyman.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * An implementation for <i>Simple Regression</i>
 *
 *
 * @author Erik Mautsch
 *
 */
public class RegressionsFunction implements Function {

    private Map<Date, Double> foreCast;
    private Calendar cal;
    private Date date;

    public RegressionsFunction() {
      foreCast = new HashMap<Date, Double>();
      cal = Calendar.getInstance();
      cal.set(2015, Calendar.JULY, 1);
      date = cal.getTime();
    }

    public Double computeValue(double[] x, double[] y) {
      foreCast = compute(x, y);
      return foreCast.get(date);
    }

    public Map<Date, Double> compute(double[] x, double[] y) {
        SimpleRegression regression = new SimpleRegression();
        // your code here ...
        for (int i=0; i < x.length; i++) {
          regression.addData(x[i], y[i]);
        }
        //
        // // displays intercept of regression line
    		// System.out.println("\t\tRegression intercept:\t" + regression.getIntercept());
    		// // displays slope of regression line
    		// System.out.println("\t\tRegression slope:\t" + regression.getSlope());
    		// // displays slope standard error
    		// System.out.println("\t\tRegr. slope std. error:\t" + regression.getSlopeStdErr());

        foreCast.put(date, regression.predict(6));
        return foreCast;
    }
}
