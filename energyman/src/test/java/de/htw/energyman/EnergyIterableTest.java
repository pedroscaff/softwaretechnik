package de.htw.energyman.energies;

import junit.framework.Test;
import static junit.framework.Assert.*;

/**
 * Unit test for EnergyIterable.
 */
public class EnergyIterableTest {
    /**
     * Asserts if EnergyIterable can iterate over a csv fromatted string.
     */
    public void test_Iterator() {
        String csvLine = "EWE Energie AG;134,95; 132,95";
        String[] csvSplited = csvLine.split(";");

        int i = 0;
        for (String word : new EnergyIterable(csvLine)) {
            assertEquals(word, csvSplited[i++]);
        }

    }
}