#!/bin/bash
COMPILE=$1
echo 'Compiling...'
javac -cp .:jUnit/junit-4.12.jar $COMPILE
echo 'Done!'
CLASS=${COMPILE/.java/}
CLASS=${CLASS//\//.}
echo 'Run tests...'
java -cp .:jUnit/junit-4.12.jar:jUnit/hamcrest-core-1.3.jar org.junit.runner.JUnitCore $CLASS
